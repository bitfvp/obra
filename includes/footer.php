<hr>

<footer class="footer bg-transparent">
    <div class="container text-center">
        <p class="">
            Departamento de TI - Prefeitura de Manhuaçu/MG
            ®<?php echo date("Y"); ?>
        </p>
    </div>
</footer>