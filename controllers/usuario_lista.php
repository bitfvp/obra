<?php

function fncusuariolist(){
    $sql = "SELECT * FROM tbl_users WHERE status=1 ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
	$consulta->bindParam(1,$_SESSION['matriz']);
    $consulta->execute();
    $usuariolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $usuariolista;
}

function fncgetusuario($id){
    $sql = "SELECT * FROM tbl_users WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getusuario = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getusuario;
}
