<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="usuariosave"){
    $pessoa=$_GET["id"];
    $nome = remover_caracter(ucwords(strtolower($_POST["nome"])));
    $status = $_POST["status"];
    $nivel_acesso = $_POST["nivel_acesso"];
    $email = strtolower($_POST["email"]);
    $nascimento = $_POST["nascimento"];
    if($nascimento==""){
        $nascimento="1000-01-01";
    }
    $cpf = limpadocumento($_POST["cpf"]);



    if(empty($nome) || empty($email) || empty($nascimento) || $nascimento==0 ){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];
    }else{
        //valida email
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
            //executa classe
            $salvar1= new Usuario();
            $salvar1->fncusuarioedit(
                $pessoa,
                $nome,
                $status,
                $nivel_acesso,
                $email,
                $nascimento,
                $cpf
            );
            header("Location: index.php");
            exit();
        }else{
            //EMAIL ERRADO
            $_SESSION['fsh']=[
                "flash"=>"Preencha corretamente o email",
                "type"=>"warning",
            ];
        }
    }
}
?>