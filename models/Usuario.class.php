<?php
class Usuario{
    public function fncnewusuario(
        $nome,
        $status,
        $nivel_acesso,
        $email,
        $nascimento,
        $cpf
    ){
        //tratamento das variaveis
        $senha=sha1("12345678"."1010011010");

        if($nascimento==""){
            $nascimento="1900-01-01";
        }
        //valida se ja ha um usuario cadastrado
        try{
            $sql="SELECT * FROM tbl_users WHERE email=:email";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":email", $email);
            $consulta->execute();

        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        $contar=$consulta->rowCount();
        if($contar==0){
            //inserção no banco
            try{
                $sql="INSERT INTO tbl_users("
                    ."nome, status, nivel_acesso, email, senha, nascimento, cpf "
                    .")VALUES("
                    .":nome, :status, :nivel_acesso, :email, :senha, :nascimento, :cpf "
                    .")";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":status", $status);
                $insere->bindValue(":nivel_acesso", $nivel_acesso);
                $insere->bindValue(":email", $email);
                $insere->bindValue(":senha", $senha);
                $insere->bindValue(":nascimento", $nascimento);
                $insere->bindValue(":cpf", $cpf);
                $insere->execute();

            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já há um usuario cadastrado com esse email em nosso sistema!!!!",
                "type"=>"danger",
            ];

        }

        if(isset($insere)){

            //seleciona o ultimo usuario cadastrado


            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
//==================================================================================================

//=========================================================================================
    public function fncusuarioedit(
        $pessoa,
        $nome,
        $status,
        $nivel_acesso,
        $email,
        $nascimento,
        $cpf
    )
    {
        try{
            $sql = "UPDATE tbl_users SET "
                ."nome = :nome, "
                ."status = :status, "
                ."nivel_acesso = :nivel_acesso, "
                ."email = :email, "
                ."nascimento = :nascimento, "
                ."cpf = :cpf "
                ."WHERE id = :id";
            global $pdo;
            $insere2=$pdo->prepare($sql);
            $insere2->bindValue(":nome", $nome);
            $insere2->bindValue(":status", $status);
            $insere2->bindValue(":nivel_acesso", $nivel_acesso);
            $insere2->bindValue(":email", $email);
            $insere2->bindValue(":nascimento", $nascimento);
            $insere2->bindValue(":cpf", $cpf);
            $insere2->bindValue(":id", $pessoa);
            $insere2->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere2)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }
/// //==================================================================================================
///
///
//=========================================================================================


//=========================================================================================
    public function fncnewsenha($id,$senha){
        //tratamento das variaveis
        $senha=sha1($senha."1010011010");

        //atualização no banco
        try{
            $sql="UPDATE tbl_users SET senha=:senha ";
            $sql.="WHERE id=:id";
            global $pdo;
            $at=$pdo->prepare($sql);
            $at->bindValue(":id", $id);
            $at->bindValue(":senha", $senha);
            $at->execute(); 
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if(isset($at)){
            $_SESSION['fsh']=[
                "flash"=>"Atualização de senha realizado com sucesso!!",
                "type"=>"success",
                "error"=>"No proximo login use a nova senha cadastrada",
            ];
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
//==================================================================================================

//=========================================================================================
    public function fncresetsenha($id){
        //inserção no banco
        try{
            $sql = "UPDATE tbl_users SET senha = :novasenha WHERE id = :id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":novasenha", "c8b27e713ebfe1e38f991c4462360a9b11242db9");
            $insere->bindValue(":id", $id);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Senha resetada com sucesso!! ",
                "type"=>"success",
                "error"=>"<h2>Nova senha: 12345678</h2>"
            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador ",
                    "type"=>"danger",
                ];

            }
        }
    }
    //==================================================================================================

//=========================================================================================
}
?>