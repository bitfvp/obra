<?php
//echo ini_get("upload_max_filesize")."<br>";
//echo ini_get("post_max_size");
?>
<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Envio de arquivos
    </div>
    <div class="card-body">
        <form action="index.php?pg=Vo&aca=arquivonew&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="descricao">Descrição da foto:</label>
                    <textarea id="descricao" onkeyup="limite_textarea(this.value,250,descricao,'cont')" maxlength="250"
                              class="form-control" rows="2" name="descricao" required></textarea>
                    <span id="cont">250</span>/250
                </div>
                <div class="form-group col-md-12">
                    <label for="arquivo">Aceito arquivos do tipo JPG,JPEG,PNG com menos de 10MB</label>
                    <input type="file" class="form-control form-control-file" id="arquivo" name="arquivo" value="" >
                </div>

                <div class="col-md-12 d-grid">
                    <input type="submit" value="ENVIAR ARQUIVOS" class="btn btn-success my-2" />
                </div>
            </div>
        </form>
    </div>
</div>