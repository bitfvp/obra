<?php
//existe um id e se ele é numérico
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $sql = "SELECT * FROM\n"
        . "tbl_obras_comentarios \n"
        . "WHERE obra = ? ORDER BY \n"
        . "data_ts ASC ";
    global $pdo;
    $consulta2 = $pdo->prepare($sql);
    $consulta2->bindParam(1,$_GET['id']);
    $consulta2->execute();
    $insc = $consulta2->fetchAll();
    $sql=null;
    $consulta2=null;
}
?>

<script>
    setTimeout(function(){
        $('#cardcomentario').collapse();
    }, 4000);
</script>
<div class="card" >
    <div class="card-header bg-info text-light" data-toggle="collapse" data-target="#cardcomentario" aria-expanded="false" aria-controls="cardcomentario">
        Comentarios da obra
    </div>
    <div class="card-body" >

            <?php
            $ccc=0;
            foreach ($insc as $ins){
                ?>
                <hr>
                <blockquote class="blockquote blockquote-info">
                    “<strong class="text-success"><?php echo $ins['mensagem']; ?></strong>”
                    <br>
                    <strong class="text-info"><?php echo datahoraBanco2data($ins['data_ts']); ?>&nbsp;&nbsp;</strong><br>
                    <strong class="text-info"><?php echo $ins['dispositivo']; ?>&nbsp;&nbsp;</strong><br>
                    <strong class="text-info"><?php echo $ins['sistema_operacional']; ?>&nbsp;&nbsp;</strong><br>
                    <strong class="text-info"><?php echo $ins['navegador']; ?>&nbsp;&nbsp;</strong><br>
                </blockquote>
            <?php } ?>
    </div>
</div>