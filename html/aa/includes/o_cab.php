<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Dados da Obra
    </div>
    <div class="card-body">
        <blockquote class="blockquote blockquote-info">
            <header>DENOMINAÇÃO: <strong class="text-info"><?php echo $ps['obra']; ?>&nbsp;&nbsp;</strong></header>
            <h6>
                <?php
                if ($ps['tipo'] == 0) {
                    echo "<strong class='text-danger'>erro</strong>";
                }
                if ($ps['tipo'] ==1) {
                    echo "<strong class='text-success'>NOVA OBRA</strong>";
                }
                if ($ps['tipo'] ==2) {
                    echo "<strong class='text-success'>REFORMA</strong>";
                }
                ?>
            </h6>
            <h6>
                <?php
                if ($ps['status'] == 0) {
                    echo "<strong class='text-danger'>INATIVO</strong>";
                }
                if ($ps['status'] ==1) {
                    echo "<strong class='text-info'>EM ANDAMENTO</strong>";
                }
                if ($ps['status'] ==2) {
                    echo "<strong class='text-success'>CONCLUÍDO</strong>";
                }
                ?>
            </h6>
            <h6>
                ENDEREÇO: <strong class="text-info"><?php echo $ps['endereco']; ?></strong>
            </h6>
            <h6>
                INÍCIO DA OBRA: <strong class="text-info"><?php echo datahoraBanco2data($ps['inicio_obra']); ?></strong>
            </h6>
            <h6>
                PREVISÃO DE CONCLUSÃO: <strong class="text-info"><?php echo $ps['previsao']; ?> dias</strong>
            </h6>

            <h6>
                VALOR DA OBRA: <strong class="text-info" id="valor1">R$<?php echo number_format($ps['valor'],2,',','.'); ?></strong>
            </h6>

            <h6>
                ADITIVO: <strong class="text-info" id="valor1">R$<?php echo number_format($ps['aditivo'],2,',','.'); ?></strong>
            </h6>

            <h6>
                VALOR JÁ EMPENHADO: <strong class="text-info" id="valor2">R$<?php echo number_format($ps['valor_parcial'],2,',','.'); ?></strong>
            </h6>
            <?php
            $max_valor=$ps['valor']+$ps['aditivo']-$ps['valor_parcial'];

            if ($ps['tipo']==1){
                //novaobra
                $s_porc=25;
            }
            if ($ps['tipo']==2){
                //reforma
                $s_porc=50;
            }
            $s_calc=($ps['valor']/100)*$s_porc;
            $max_aditivo=$s_calc-$ps['aditivo'];
            ?>
            <h6>
                <strong class="text-danger" id="valor3"><?php $porc=$ps['valor_parcial']*100/($ps['valor']+$ps['aditivo']); echo number_format($porc,2);?>% DA OBRA CONCLUÍDA</strong>
            </h6>

            <h6>
                ORIGEM DO RECURSO: <strong class="text-info"><?php echo $ps['recursos']; ?></strong>
            </h6>

            <h6>
                ORDENADOR DE DESPESAS: <strong class="text-info"><?php echo $ps['orgao_gestor']; ?></strong>
            </h6>

            <h6>
                EXECUTOR: <strong class="text-info"><?php echo $ps['orgao_executor']; ?></strong>
            </h6>
        </blockquote>
        <a class="btn btn-success d-grid my-2" href="index.php?pg=Vo_editar&id=<?php echo $ps['id']; ?>" title="Edite os dados dessa obra">
            EDITAR
        </a>
    </div>
</div>