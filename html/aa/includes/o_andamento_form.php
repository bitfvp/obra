
        <form action="?pg=Vo&id=<?php echo $_GET['id']; ?>&aca=newandamento" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="descricao">Descrição:</label>
                    <textarea id="descricao" onkeyup="limite_textarea(this.value,1000,descricao,'cont')" maxlength="1000"
                              class="form-control" rows="2" name="descricao" required></textarea>
                    <span id="cont">1000</span>/1000
                </div>
                <div class="col-md-6">
                    <label for="valor">Valor: <i class="text-info"></i></label>
                    <input id="valor" type="text" class="form-control" name="valor" value=""/>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#valor').mask('000.000.000,00', {reverse: true});
                    });
                </script>

                <div class="col-md-6">
                    <label for="valor_maximo">Valor máximo:</label>
                    <input readonly="readonly" id="valor_maximo" type="text" class="form-control" name="valor_maximo" value="<?php echo $max_valor * 100;?>"/>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#valor_maximo').mask('000.000.000,00', {reverse: true});
                    });
                </script>

                <div class="col-md-12 d-grid">
                    <input type="submit" value="SALVAR" class="btn btn-success my-2" />
                </div>

            </div>
        </form>