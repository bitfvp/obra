<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

//reports
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/obra/vendor/autoload.php") ;
if ($pathFile) {
$realroot=$_SERVER['DOCUMENT_ROOT']."/obra/";
} else {
$realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();
/*
*   inclui e inicia a classe de configuração de ambiente que é herdada de uma classe abaixo da raiz em models/Env.class.php
*   $env e seus atributos sera usada por todo o sistema
*/
include_once("models/ConfigMod.class.php");
$env=new ConfigMod();

/*
*   inclui e inicia a classe de configuração de drive pdo/mysql
*   $pdo e seus atributos sera usada por todas as querys
*/
include_once("{$env->env_root}models/Db.class.php");////conecção com o db
$pdo = Db::conn();


use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Device;
use Sinergi\BrowserDetector\Language;
$browser = new Browser();
$os = new Os();
$device = new Device();
$language = new Language();


//inclui um controle contendo a funcão que apaga a sessao e desloga
//será chamada da seguinte maneira
//killSession();
include_once("{$env->env_root}controllers/Ks.php");//se ha uma acao

//inclui um controle que ativa uma acao
include_once("{$env->env_root}controllers/action.php");//se ha uma acao

//funcoes diversas
include_once ("{$env->env_root}includes/funcoes.php");//funcoes

//valida o token e gera array
include_once("{$env->env_root}controllers/validaToken.php");

include_once ("{$env->env_root}includes/funcoes_diarias.php");//funcoes diarias


//usuario();
include_once("{$env->env_root}models/Usuario.class.php");
include_once("{$env->env_root}controllers/usuario_lista.php");
include_once("{$env->env_root}controllers/usuario_trocasenha.php");
//include_once("{$env->env_root}controllers/usuario_edit.php");

//obra
include_once("models/Obra.class.php");
include_once("controllers/o_novo.php");
include_once("controllers/o_edicao.php");
include_once("controllers/o_carregar.php");


//arquivos
include_once("controllers/arquivos_novo.php");
include_once("controllers/arquivos_edit.php");
include_once("controllers/arquivos_apaga.php");


//andamento
include_once("models/Andamento.class.php");
include_once("controllers/andamento_novo.php");


//logout
//controle pra logout
include_once("{$env->env_root}controllers/logout.php");

//validação de sessoes ativas
//inclui um controle que valida o hash e renova o mesmo se tiver tudo certo
include_once("{$env->env_root}controllers/validaToken.php");


//metodo de checar usuario, confirma se a session que vincula o login ao hash de sessao no banco de dados esta ativa
include_once("{$env->env_root}controllers/confirmUser.php");