<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Arquivos de imagem
    </div>
    <div class="card-body">
        <blockquote class="blockquote blockquote-info">
            <div class="row">
                <?php
                $sql = "SELECT * FROM `tbl_obras_arquivos` where obra='{$_GET['id']}' and tipo='arquivos' ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute();
                $dados = $consulta->fetchAll();//$total[0]
                $sql = null;
                $consulta = null;

                foreach ($dados as $dado){
                    $status="";
                    if ($dado['status']==0){
                        $status="bg-danger d-none ";
                    }
                    $link="../dados/" . $dado['obra'] . "/" . $dado['tipo'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                    $caminho=$link;
                    echo "<div class='col-md-12 text-center {$status}'>";
                    echo "<a href=" . $link . " target='_blank'>";
                    echo "<img src=" . $caminho . " alt='...'  class='img-fluid img-thumbnail'>";
                    echo "</a>";
                    ?>
                    <form action="index.php?pg=Vo&id=<?php echo $_GET['id'];?>&aca=arquivosave&ida=<?php echo $dado['id'];?>" method="post" name="<?php echo $dado['id'];?>">
                        <input type="hidden">

                        <div class="form-group col-md-12">
                            <textarea id="descricao" maxlength="250"
                                      class="form-control" rows="1" name="descricao" required><?php echo $dado['descricao'];?></textarea>
                        </div>

                        <div class="col-md-12 d-grid">
                            <input type="submit" value="SALVAR" class="btn btn-success my-2" />
                        </div>

                    </form>


                    <?php
//                    echo "<figcaption class='blockquote-footer small m-0 lh-1'>";
//                    echo $dado['descricao'];
//                    echo "</figcaption>";
                    echo "<a href=" . $link . " target='_blank' title='View larger' class='btn btn-sm'><i class='fas fa-search-plus fa-2x'></i></a>";

                    if ($dado['home']==1){
                        $us=fncgetusuario($dado['delete_prof']);
                        echo "<h6 class='fas fa-home text-success'></h6>";
                    }else{
                        $act="index.php?pg=Vo&id=".$_GET['id'] ."&aca=marcaarquivo&obra=". $dado['obra'] . "&ft=". $dado['arquivo'];
                        echo "<a href='{$act}' target='_self' title='Delete image' class='btn btn-sm'><i class='fas fa-home fa-2x'></i></a>";
                    }

                    if ($dado['status']==0){
                        $us=fncgetusuario($dado['delete_prof']);
                        echo "<h6 class='fas fa-dizzy text-dark'>Desativado por ".$us['nome']."</h6>";
                    }else{
                        $act="index.php?pg=Vo&id=".$_GET['id'] ."&aca=apagaarquivo&obra=". $dado['obra'] . "&ft=". $dado['arquivo'];
                        echo "<a href='{$act}' target='_self' title='Delete image' class='btn btn-sm'><i class='fas fa-trash-alt fa-2x'></i></a>";
                    }
                    echo "</div>";
                }
                ?>
            </div>
            <br>
            <footer class="blockquote-footer text-danger">
                Click no arquivo pra acessar
            </footer>
        </blockquote>
    </div>
</div>