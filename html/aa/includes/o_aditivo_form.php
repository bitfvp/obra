
        <form action="?pg=Vo&id=<?php echo $_GET['id']; ?>&aca=newaditivo" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="descricaob">Descrição:</label>
                    <textarea id="descricaob" onkeyup="limite_textarea(this.value,1000,descricaob,'contb')" maxlength="1000"
                              class="form-control" rows="2" name="descricaob"></textarea>
                    <span id="contb">1000</span>/1000
                </div>
                <div class="col-md-6">
                    <label for="valorb">Valor: <i class="text-info"></i></label>
                    <input id="valorb" type="text" class="form-control" name="valorb" value=""/>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#valorb').mask('000.000.000,00', {reverse: true});
                    });
                </script>

                <div class="col-md-6">
                    <label for="valor_maximob">Valor máximo:</label>
                    <input readonly="readonly" id="valor_maximob" type="text" class="form-control" name="valor_maximob" value="<?php echo $max_aditivo * 100;?>"/>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#valor_maximob').mask('000.000.000,00', {reverse: true});
                    });
                </script>

                <div class="col-md-12 d-grid">
                    <input type="submit" value="SALVAR" class="btn btn-success my-2" />
                </div>

            </div>
        </form>