<?php
//existe um id e se ele é numérico
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    // Captura os dados do cliente solicitado
    $sql = "SELECT * "
        . "FROM tbl_obras_andamento "
        . "WHERE obra=? order by data_cadastro desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute();
    $ativi = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
}
?>

<div id="pointofview" class="card">
    <div class="card-header bg-info text-light">
        Histórico
    </div>
    <div class="card-body">
        <h5>
            <?php
            foreach ($ativi as $at) {

                if ($at['tipo']==1){$cor="warning";}
                if ($at['tipo']==2){$cor="danger";}
                ?>
                <hr>
                <blockquote class="blockquote blockquote-<?php echo $cor;?>">
                    “<strong class="text-success"><?php echo $at['descricao']; ?></strong>”
                    <br>
                    <strong class="text-info"><?php echo datahoraBanco2data($at['data_cadastro']); ?>&nbsp;&nbsp;</strong><br>
                    <strong class="text-info">R$<?php echo number_format($at['valor'],2,',','.');?>&nbsp;&nbsp;</strong>
                    <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                    <br>
                    <br>
                    <footer class="blockquote-footer">
                        <?php
                        $us=fncgetusuario($at['profissional']);
                        echo $us['nome'];
                        ?>
                    </footer>

                </blockquote>
                <?php
            }
            ?>
        </h5>
    </div>
</div>