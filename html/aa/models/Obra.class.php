<?php
class Obra{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncobranew($profisional,$obra,$tipo,$endereco,$inicio_obra,$previsao,$valor,$orgao_gestor,$orgao_executor,$recursos,$status){

            //inserção no banco
            try{
                $sql="INSERT INTO tbl_obras ";
                $sql.="(id, profissional, obra, tipo, endereco, inicio_obra, previsao, valor, orgao_gestor, orgao_executor, recursos, status)";
                $sql.=" VALUES ";
                $sql.="(NULL, :profissional, :obra, :tipo, :endereco, :inicio_obra, :previsao, :valor, :orgao_gestor, :orgao_executor, :recursos, :status)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":profissional", $profisional);
                $insere->bindValue(":obra", $obra);
                $insere->bindValue(":tipo", $tipo);
                $insere->bindValue(":endereco", $endereco);
                $insere->bindValue(":inicio_obra", $inicio_obra);
                $insere->bindValue(":previsao", $previsao);
                $insere->bindValue(":valor", $valor);
                $insere->bindValue(":orgao_gestor", $orgao_gestor);
                $insere->bindValue(":orgao_executor", $orgao_executor);
                $insere->bindValue(":recursos", $recursos);
                $insere->bindValue(":status", $status);
                $insere->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vo_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncobraedit($id,$obra,$tipo,$endereco,$inicio_obra,$previsao,$valor,$orgao_gestor,$orgao_executor,$recursos,$status){

        //inserção no banco
        try {
            $sql="UPDATE tbl_obras SET obra=:obra, tipo=:tipo, endereco=:endereco, inicio_obra=:inicio_obra, "
                ."previsao=:previsao, valor=:valor, orgao_gestor=:orgao_gestor, orgao_executor=:orgao_executor, recursos=:recursos, status=:status "
                ."WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":obra", $obra);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":endereco", $endereco);
            $insere->bindValue(":inicio_obra", $inicio_obra);
            $insere->bindValue(":previsao", $previsao);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":orgao_gestor", $orgao_gestor);
            $insere->bindValue(":orgao_executor", $orgao_executor);
            $insere->bindValue(":recursos", $recursos);
            $insere->bindValue(":status", $status);
            $insere->bindValue(":id", $id);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vo&id={$id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}