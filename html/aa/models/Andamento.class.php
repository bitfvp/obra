<?php
class Andamento
{
    public function fncandamentonew($obra,$tipo,$id_prof,$descricao,$valor)
    {

        //inserção no banco
        try {
            $sql = "INSERT INTO tbl_obras_andamento ";
            $sql .= "(id, obra, tipo, data_cadastro, profissional, descricao, valor)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :obra, :tipo, CURRENT_TIMESTAMP, :profissional, :descricao, :valor)";

            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":obra", $obra);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":profissional", $id_prof);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":valor", $valor);
            $insere->execute();

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        try {
            $sql = "SELECT valor_parcial FROM tbl_obras WHERE id=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$obra);
            $consulta->execute();
            $getobra = $consulta->fetch();
            $sql=null;
            $consulta=null;
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }
//        echo $getobra[0];

        $novo_valor=$getobra[0]+$valor;


        try {
            $sql="UPDATE tbl_obras SET valor_parcial=:valor_parcial "
                ."WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":valor_parcial", $novo_valor);
            $insere->bindValue(":id", $obra);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }




        if (isset($insere)) {
            /////////////////////////////////////////////////////
            //criar log
            //reservado ao log
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"andamento Cadastrado com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }







    public function fncaditivonew($obra,$tipo,$id_prof,$descricao,$valor)
    {

        //inserção no banco
        try {
            $sql = "INSERT INTO tbl_obras_andamento ";
            $sql .= "(id, obra, tipo, data_cadastro, profissional, descricao, valor)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :obra, :tipo, CURRENT_TIMESTAMP, :profissional, :descricao, :valor)";

            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":obra", $obra);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":profissional", $id_prof);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":valor", $valor);
            $insere->execute();

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        try {
            $sql = "SELECT aditivo FROM tbl_obras WHERE id=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$obra);
            $consulta->execute();
            $getobra = $consulta->fetch();
            $sql=null;
            $consulta=null;
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }
//        echo $getobra[0];

        $novo_valor=$getobra[0]+$valor;


        try {
            $sql="UPDATE tbl_obras SET aditivo=:aditivo "
                ."WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":aditivo", $novo_valor);
            $insere->bindValue(":id", $obra);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }




        if (isset($insere)) {
            /////////////////////////////////////////////////////
            //criar log
            //reservado ao log
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Aditivo Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }




}
