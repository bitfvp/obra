<?php
if (!isset($_SESSION["logado"]) and ($_SESSION['nivel_acesso']==0 or $_SESSION['nivel_acesso']==1)) {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vl");
    exit();
}else{

}
$page="Alterar Senha-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-info text-light">
                Alteração de senha de acesso ao sistema
            </div>
            <div class="card-body">
                <form action="?pg=Vtroca_senha&aca=novasenha" method="post">
                    <div class="col-md-12">
                        <label for="senha">Nova senha</label>
                        <input type="password" class="form-control" name="newpass"  autocomplete="off" placeholder="Digite a nova senha com pelo menos 6 caracteres" autofocus minlength="6" maxlength="30"/>
                    </div>

                    <div class="col-md-12 d-grid">
                        <input type="submit" value="SALVAR NOVA SENHA" class="btn btn-success my-2" />
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div class="col-md-3"></div>
</div>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>