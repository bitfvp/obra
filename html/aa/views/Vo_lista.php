<?php
if(isset($_SESSION['logado']) and ($_SESSION['nivel_acesso']==0 or $_SESSION['nivel_acesso']==1)){
    //acesso liberado
}else{
    header("Location: {$env->env_url}?pg=Vl");
    exit();;
}

$page="Lista de Obras-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    //consulta se ha busca
    if ($_SESSION['nivel_acesso']==1){
        $sql = "select * from tbl_obras WHERE obra LIKE '%$sca%'  ";
    }else{
        $sql = "select * from tbl_obras WHERE obra LIKE '%$sca%' and profissional={$_SESSION['id']}  ";
    }


}else {
//consulta se nao ha busca
    if ($_SESSION['nivel_acesso']==1){
        $sql = "select * from tbl_obras ";
    }else{
        $sql = "select * from tbl_obras WHERE profissional={$_SESSION['id']}  ";
    }

}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY data_ts desc LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY data_ts desc LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <h2>Listagem de obras</h2>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <form action="index.php" method="get">
                <div class="input-group mb-3">
                    <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
                    <input name="pg" value="Vo_lista" hidden/>
                    <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por obra..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <a href="index.php?pg=Vo_editar" class="btn btn-success d-grid">
                NOVA OBRA
            </a>
            <script type="text/javascript">
                function selecionaTexto()
                {
                    document.getElementById("sca").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
    </div>



    <table class="table table-striped table-hover table-sm table-responsive">
        <thead>
            <tr>
                <th>STATUS</th>
                <th>OBRA</th>
                <th>INÍCIO</th>
                <th>PRAZO DA OBRA</th>
                <th>EDITAR</th>
            </tr>
        </thead>
        <tbody>
        <?php
        // vamos criar a visualização
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $obra = $dados["obra"];
            $inicio_obra = dataRetiraHora($dados["inicio_obra"]);
            $previsao = $dados["previsao"];
            if ($dados["status"]==0){
                $status = "<i class='text-danger'>INATIVO</i>";
            }
            if ($dados["status"]==1){
                $status = "<i class='text-info'>EM ANDAMENTO</i>";
            }
            if ($dados["status"]==2){
                $status = "<i class='text-success'>CONCLUÍDO</i>";
            }
            ?>
            <tr>
                <td><?php echo $status; ?></td>
                <td>
                    <a href="index.php?pg=Vo&id=<?php echo $id; ?>">
                    <?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $ccc = $obra;
                        $cc = explode(CSA, $ccc);
                        $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                        echo $c;
                    }else{
                        echo $obra;
                    }
                    ?>
                    </a>
                </td>
                <td><?php echo $inicio_obra; ?></td>
                <td><?php echo $previsao; ?> dias</td>
                <td>
                    <a href="index.php?pg=Vo_editar&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
