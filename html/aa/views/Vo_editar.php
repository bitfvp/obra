<?php
if(isset($_SESSION['logado']) and ($_SESSION['nivel_acesso']==0 or $_SESSION['nivel_acesso']==1)){
    //acesso liberado
}else{
    header("Location: {$env->env_url}?pg=Vl");
    exit();;
}

$page="Editar obra-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="obrasave";
    $ps=fncgetobra($_GET['id']);
    //verificar se tem permissao
    if (($_SESSION['nivel_acesso']!=1) and $ps['profissional']!=$_SESSION['id']){
        header("Location: index.php?pg=Vo_lista&id={$_GET['id']}");
        exit();
    }
}else{
    $a="obranew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vo_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de Obra</h3>
        <hr>
        <div class="row">
            <div class="col-md-9">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $ps['id']; ?>"/>
                <label for="obra">Denominação:</label>
                <input autocomplete="off" id="obra" placeholder="" type="text" class="form-control" name="obra" value="<?php echo $ps['obra']; ?>"/>
            </div>
            <div class="col-md-3">
                <label for="tipo">Tipo:</label>
                <select name="tipo" id="tipo" class="form-control" required>
                    <option selected="" value="<?php if ($ps['tipo'] == "") {
                        echo null;
                    } else {
                        echo $ps['tipo'];
                    } ?>">
                        <?php
                        if ($ps['tipo'] == null) {
                            echo "Selecione...";
                        }

                        if ($ps['tipo'] == 1) {
                            echo "NOVA OBRA";
                        }
                        if ($ps['tipo'] == 2) {
                            echo "REFORMA";
                        }
                        ?>
                    </option>
                    <option value="1">NOVA OBRA</option>
                    <option value="2">REFORMA</option>
                </select>
            </div>
            <div class="col-md-12">
                <label for="endereco">Endereço:</label>
                <input autocomplete="off" id="endereco" placeholder="" type="text" class="form-control" name="endereco" value="<?php echo $ps['endereco']; ?>"/>
            </div>
            <div class="col-md-4">
                <label for="inicio_obra">Inicio da Obra:</label>
                <input id="inicio_obra" type="date" class="form-control" name="inicio_obra" value="<?php echo $ps['inicio_obra']; ?>"/>
            </div>
            <div class="col-md-4">
                <label for="previsao">Previsão: <i class="text-info"> *Em dias</i></label>
                <input id="previsao" type="text" class="form-control" name="previsao" value="<?php echo $ps['previsao']; ?>"/>
            </div>
            <script>
                $(document).ready(function(){
                    $('#previsao').mask('00000', {reverse: false});
                });
            </script>

            <div class="col-md-4">
                <label for="valor">Valor da Obra: <i class="text-info"></i></label>
                <input id="valor" type="text" class="form-control" name="valor" value="<?php echo $ps['valor']*100; ?>"/>
            </div>
            <script>
                $(document).ready(function(){
                    $('#valor').mask('000.000.000,00', {reverse: true});
                });
            </script>

            <div class="col-md-6">
                <label for="orgao_gestor">Ordenador de Despesas:</label>
                <input autocomplete="off" id="orgao_gestor" placeholder="" type="text" class="form-control" name="orgao_gestor" value="<?php echo $ps['orgao_gestor']; ?>"/>
            </div>
            <div class="col-md-6">
                <label for="orgao_executor">Executor da Obra:</label>
                <input autocomplete="off" id="orgao_executor" placeholder="" type="text" class="form-control" name="orgao_executor" value="<?php echo $ps['orgao_executor']; ?>"/>
            </div>

            <div class="col-md-6">
                <label for="recursos">Recursos:</label>
                <input autocomplete="off" id="recursos" placeholder="" type="text" class="form-control" name="recursos" value="<?php echo $ps['recursos']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="status">Status:</label>
                <select name="status" id="status" class="form-control">
                    <option selected="" value="<?php if ($ps['status'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $ps['status'];
                    } ?>">
                        <?php
                        if ($ps['status'] == 0) {
                            echo "INATIVO";
                        }
                        if ($ps['status'] ==1) {
                            echo "EM ANDAMENTO";
                        }
                        if ($ps['status'] ==2) {
                            echo "CONCLUÍDO";
                        }
                        ?>
                    </option>
                    <option value="0">INATIVO</option>
                    <option value="1">EM ANDAMENTO</option>
                    <option value="2">CONCLUÍDO</option>
                </select>
            </div>

            <div class="col-md-12 d-grid">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>