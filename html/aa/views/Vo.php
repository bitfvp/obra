<?php
if(isset($_SESSION['logado']) and ($_SESSION['nivel_acesso']==0 or $_SESSION['nivel_acesso']==1)){
    //acesso liberado
}else{
    header("Location: {$env->env_url}?pg=Vl");
    exit();;
}

$page="Obra-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $ps=fncgetobra($_GET['id']);
//verificar se tem permissao
if (($_SESSION['nivel_acesso']!=1) and $ps['profissional']!=$_SESSION['id']){
    header("Location: index.php?pg=Vo_lista&id={$_GET['id']}");
    exit();
}

}else{
    echo "HOUVE ALGUM ERRO";
    exit();
}
?>
    <div class="row">
        <div class="col-md-6"><!-- lado esquerdo-->
            <div class="row">
                <div class="col-md-12">
                    <?php include_once("includes/o_cab.php");?>
                </div>

                 <div class="col-md-12">
                     <ul class="nav nav-pills mb-3"" id="myTab" role="tablist">
                         <li class="nav-item" role="etapa">
                             <button class="nav-link active" id="etapa-tab" data-bs-toggle="tab" data-bs-target="#etapa" type="button" role="tab" aria-controls="etapa" aria-selected="true">Lançamento de etapas da obra</button>
                         </li>
                         <li class="nav-item" role="aditivo">
                             <button class="nav-link" id="aditivo-tab" data-bs-toggle="tab" data-bs-target="#aditivo" type="button" role="tab" aria-controls="aditivo" aria-selected="false">Lançamento de aditivos</button>
                         </li>
                     </ul>
                     <div class="tab-content mb-3" id="myTabContent">
                         <div class="tab-pane fade show active" id="etapa" role="tabpanel" aria-labelledby="etapa-tab">
                             <?php include_once("includes/o_andamento_form.php");?>
                         </div>
                         <div class="tab-pane fade" id="aditivo" role="tabpanel" aria-labelledby="aditivo-tab">
                             <?php include_once("includes/o_aditivo_form.php");?>
                         </div>
                     </div>


                 </div>

                <div class="col-md-12">
                    <?php include_once("includes/o_andamento.php");?>
                </div>
            </div><!-- row fim-->

        </div><!-- lado esquerdo fim-->

        <div class="col-md-6"><!-- lado direiro-->
            <div class="row">
                <div class="col-md-12">
                    <?php include_once("includes/o_arquivos_form.php");?>
                </div>

                <div class="col-md-12">
                    <?php include_once("includes/o_arquivos.php");?>
                </div>


                <div class="col-md-12">
                    <?php include_once("includes/o_comentarios.php");?>
                </div>



            </div><!-- row fim-->
        </div><!-- lado direiro fim-->









        <div class="col-md-6">

        </div>





    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>