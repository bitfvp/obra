<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="newandamento"){
    $obra=$_GET['id'];
    $id_prof=$_SESSION["id"];
    $descricao=$_POST['descricao'];
    $valor=limpadocumento($_POST["valor"])/100;
    $valor_maximo=limpadocumento($_POST["valor_maximo"])/100;
    $tipo=1;
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($descricao) or $valor>$valor_maximo){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos corretamente!!",
            "type"=>"warning",
        ];
        header("Location: ?pg=Vo&id={$obra}");
        exit();
    }else{
        //executa classe cadastro
        $salvar= new Andamento();
        $salvar->fncandamentonew($obra,$tipo,$id_prof,$descricao,$valor);
        header("Location: ?pg=Vo&id={$obra}");
        exit();
    }
}



if($startactiona==1 && $aca=="newaditivo"){
    $obra=$_GET['id'];
    $id_prof=$_SESSION["id"];
    $descricao=$_POST['descricaob'];
    $valor=limpadocumento($_POST["valorb"])/100;
    $valor_maximo=limpadocumento($_POST["valor_maximob"])/100;
    $tipo=2;
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($descricao) or $valor>$valor_maximo){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos corretamente!!",
            "type"=>"warning",
        ];
        header("Location: ?pg=Vo&id={$obra}");
        exit();
    }else{
        //executa classe cadastro
        $salvar= new Andamento();
        $salvar->fncaditivonew($obra,$tipo,$id_prof,$descricao,$valor);
        header("Location: ?pg=Vo&id={$obra}");
        exit();
    }
}