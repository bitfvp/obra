<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-primary mb-2">
    <nav class='container'>
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="<?php echo $env->env_nome; ?>" style="max-height: 80px">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav me-auto mb-lg-0">

                <li class="nav-item">
                    <a class="nav-link" href="index.php?pg=Vhome"><i class="fas fa-search"></i></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navcon" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        CONSULTAS
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navcon">
                        <li>
                            <a class="dropdown-item" href="index.php?pg=Vusers">usuarios</a>
                        </li>
                    </ul>
                </li>

            </ul>

            <ul class="navbar-nav ms-auto mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">
                        <?php
                        $primeiroNome = explode(" ", $_SESSION["nome"]);
                        echo "Olá ".$primeiroNome[0].", ".Comprimentar();
                        ?>
                    </a>
                </li>


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarUser">
                        <li>
                            <a class="dropdown-item" href="<?php echo $env->env_url; ?>?pg=Vl""><i class="fas fa-undo"></i>&nbsp;Voltar</a>
                        </li>
                        <li><hr class="dropdown-divider"></li>
                        <li>
                            <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </nav>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</nav>