<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vl");
    exit();
}else{
    if ($_SESSION["nivel_acesso"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vl");
        exit();
    }else{

    }
}

$page="Home -".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

    if (isset($_GET['sca'])){
        $sca = $_GET['sca'];
        //consulta se ha busca
        $sql = "select * from tbl_users WHERE nome LIKE '%$sca%' ";
    }else {
//consulta se nao ha busca
        $sql = "select * from tbl_users WHERE id=0 ";
    }
    // total de registros a serem exibidos por página
    $total_reg = "30"; // número de registros por página
    //Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
    $pgn=$_GET['pgn'];
    if (!$pgn) {
        $pc = "1";
    } else {
        $pc = $pgn;
    }
    //Vamos determinar o valor inicial das buscas limitadas
    $inicio = $pc - 1;
    $inicio = $inicio * $total_reg;
    //Vamos selecionar os dados e exibir a paginação
    //limite
    try{
        $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
        global $pdo;
        $limite=$pdo->prepare($sql.$sql2);
        $limite->execute();
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    //todos
    try{
        $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
        global $pdo;
        $todos=$pdo->prepare($sql);
        $todos->execute();
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $tr=$todos->rowCount();// verifica o número total de registros
    $tp = $tr / $total_reg; // verifica o número total de páginas
    ?>
<main class="container"><!--todo conteudo-->
    <h2>Listagem de Usuários</h2>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <form action="index.php" method="get">
                <div class="input-group mb-3">
                    <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
                    <input name="pg" value="Vhome" hidden/>
                    <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por usuário..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <a href="index.php?pg=Valloweditar" class="btn btn-success d-grid">
                NOVO USUÁRIO
            </a>
            <script type="text/javascript">
                function selecionaTexto()
                {
                    document.getElementById("sca").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
    </div>


    <table class="table table-stripe table-hover table-condensed table-striped">
            <thead>
            <tr>
                <th scope="col">NOME</th>
                <th scope="col">EMAIL</th>
                <th scope="col">STATUS</th>
                <th scope="col">TIPO</th>
                <th scope="col">EDITAR</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th scope="row">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                    <?php
                    // agora vamos criar os botões "Anterior e próximo"
                    $anterior = $pc -1;
                    $proximo = $pc +1;
                    if ($pc>1) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&pgn={$anterior}&sca={$_GET['sca']}' ><span aria-hidden=\"true\">&laquo; Anterior</a></li> ";
                    }
                    echo "|";
                    if ($pc<$tp) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&pgn={$proximo}&sca={$_GET['sca']}' >Próximo &#187;</a></li>";
                    }
                    ?>
                        </ul>
                    </nav>
                </th>
                <td colspan="5" class="text-right text-info"><?php echo $tr;?> Cadastro(s) listado(s)</td>
            </tr>
            </tfoot>

            <tbody>
            <?php
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);
            foreach ($limite as $dados){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $email = $dados["email"];
            $completo = $dados["completo"];
            $status = $dados["status"];
            $nivel_acesso = $dados["nivel_acesso"];
            ?>
            <tr>
                <th scope="row" id="<?php echo $id;  ?>">
                    <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                        <?php
                        if($_GET['sca']!="") {
                            $sta = CSA;
                            $ccc = strtoupper($nome);
                            $cc = explode(CSA, $ccc);
                            $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                            echo $c;
                        }else{
                            echo $nome;
                        }
                        ?>
                    </a>
                </th>
                <td>
                    <?php
                        echo $email;
                    ?>
                </td>

                <td>
                    <?php
                    if ($status==1){
                        echo "<i class='text-success fa fa-hand-peace'>Ativo</i>";
                    }else{
                        echo "<i class='text-danger fa fa-thumbs-down'>Desativado</i>";
                    }
                    ?>
                </td>

                <td>
                    <?php
                    if ($nivel_acesso==0){
                        echo "<i class='text-info'>Comum</i>";
                    }else{
                        echo "<i class='text-success'>Admin</i>";
                    }
                    ?>
                </td>

                <td>
                    <a href='index.php?pg=Valloweditar&id=<?php echo $id;?>' title='Edite as permissões desse Cadastro'>Alterar</a>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
