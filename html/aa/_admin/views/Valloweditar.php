<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vl");
    exit();
}else{
    if ($_SESSION["nivel_acesso"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vl");
        exit();
    }else{

    }
}

$page="Editar Allow-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="usuariosave";
    $us_er=fncgetusuario($_GET['id']);
}else{
    $a="usuarionew";
}
?>


<main class="container"><!--todo conteudo-->

        <form class="frmgrid" action="index.php?pg=Valloweditar&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post">


        <div class="row">
            <div class="col-md-6 d-grid">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
            <div class="col-md-6 d-grid">
                <a href="index.php?pg=Valloweditar&id=<?php echo $_GET['id'];?>&aca=resetsenha" class="btn btn-danger my-2">REINICIAR SENHA DO USUÁRIO</a>
            </div>
        </div>

            <hr>
            <div class="row">
                <div class="col-md-5">
                    <label  class="large" for="nome">NOME:</label>
                    <input autocomplete="off" autofocus id="nome" type="text" class="form-control" name="nome" value="<?php echo $us_er['nome']; ?>" required/>
                </div>

                <div class="col-md-3">
                    <label  class="large" for="">E-MAIL<span>*:</span></label>
                    <input autocomplete="off" id="email" type="email" class="form-control" name="email" value="<?php echo $us_er['email']; ?>" required/>
                </div>

                <div class="col-md-3">
                    <label  class="large" for="">NASCIMENTO:</label>
                    <input name="nascimento" id="nascimento" type="date" class="form-control" value="<?php echo $us_er['nascimento']; ?>" required/>
                </div>

                <div class="col-md-3">
                    <label  class="large" for="">CPF:</label>
                    <input name="cpf" id="cpf" type="text" class="form-control" value="<?php echo $us_er['cpf']; ?>" required/>
                    <script>
                        $(document).ready(function(){
                            $('#cpf').mask('000.000.000-00', {reverse: false});
                        });
                    </script>
                </div>

                <div class="col-md-3">
                    <label  class="x-small" for="status">STATUS:</label>
                    <select name="status" id="status" class="form-control" >
                        // vamos criar a visualização de rf
                        <option selected="" value="<?php if($us_er['status']==""){$z=0; echo $z;}else{ echo $us_er['status'];} ?>">
                            <?php
                            if($us_er['status']==0){echo"DESATIVADO";}
                            if($us_er['status']==1){echo"ATIVO";} ?>
                        </option>
                        <option value="0">DESATIVADO</option>
                        <option value="1">ATIVO</option>
                    </select>
                </div>

                <div class="col-md-3">
                    <label  class="x-small" for="nivel_acesso">NIVEL DE ACESSO:</label>
                    <select name="nivel_acesso" id="nivel_acesso" class="form-control" >
                        // vamos criar a visualização de rf
                        <option selected="" value="<?php if($us_er['nivel_acesso']==""){$z=0; echo $z;}else{ echo $us_er['nivel_acesso'];} ?>">
                            <?php
                            if($us_er['nivel_acesso']==0){echo"NORMAL";}
                            if($us_er['nivel_acesso']==1){echo"ADMINISTRADOR";} ?>
                        </option>
                        <option value="0">NORMAL</option>
                        <option value="1">ADMINISTRADOR</option>
                    </select>
                </div>

            </div>
  
</form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>