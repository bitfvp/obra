<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vl");
    exit();
}else{
    if ($_SESSION["nivel_acesso"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vl");
        exit();
    }else{

    }
}


$page="Acessos ao sistema-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->
            <?php
            // Prepare a select statement
            $sql = "SELECT * FROM tbl_users  ORDER BY nome ASC ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $frase = $consulta->fetchall();
            $cont = $consulta->rowCount();
            $sql=null;
            $consulta=null;
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Dados
                </div>
                <div class="card-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th scope="col">NOME</th>
                            <th scope="col">TIPO</th>
                            <th scope="col">STATUS</th>
                            <th scope="col">E-MAIL</th>
                            <th scope="col">NASCIMENTO</th>
                            <th scope="col">CPF</th>

                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach ($frase as $tx){
                            if ($tx['nivel_acesso']==0){
                                $tipo="<i class='text-info'>Normal</i>";
                            }
                            if ($tx['nivel_acesso']==1){
                                $tipo="<i class='text-success'>Administrador</i>";
                            }

                            if ($tx['status']==0){
                                $status="<i class='text-danger'>Inativo</i>";
                            }
                            if ($tx['status']==1){
                                $status="<i class='text-success'>Ativo</i>";
                            }

                            echo "<tr class='text-dark'>";
                            echo "<td>{$tx['nome']}</td>";
                            echo "<th>{$tipo}</th>";
                            echo "<th>{$status}</th>";
                            echo "<th>{$tx['email']}</th>";

                            echo "<th>";
                            if($tx['nascimento']!="1900-01-01" and $tx['nascimento']!="" and $tx['nascimento']!="1000-01-01") {
                                echo "<span class='text-info'>";
                                echo dataBanco2data ($tx['nascimento']);
                                echo " <i class='text-success'>".Calculo_Idade($tx['nascimento'])." anos</i>";
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            echo "</th>";

                            echo "<th>";
                            if($tx['cpf']!="") {
                                echo "<span class='text-info'>";
                                echo mask($tx['cpf'],'###.###.###-##');
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            echo "</th>";

                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>