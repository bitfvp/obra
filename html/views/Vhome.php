<?php
$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");




?>
<style>
    .fundo-1{
        background: #a7b1cc;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #e57e5f, #bc9397, #a7b1cc);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #e57e5f, #bc9397, #a7b1cc); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background-size: 100%;
    }
</style>
<main class="container-fluid fundo-1">

    <header id="head" class="container text-center mb-5 bg-transparent">
        <a href="index.php"><img class="img-fluid img-logo mt-2" src="<?php echo $env->env_estatico; ?>img/PREFEITURA_AZUL.png" alt="" title="<?php echo $env->env_nome; ?>"/></a><br>
        <a href="index.php"><img class="img-fluid img-logo mt-2" src="<?php echo $env->env_estatico; ?>img/Obrometro2azul.png" alt="" title="<?php echo $env->env_nome; ?>"/></a>
        <br>
        <br>
        <br>
        <br>
        <H1 class="fontpref2021 fontcollorpref2021 text-uppercase">CONFIRA AQUI O STATUS DAS OBRAS MUNICIPAIS</H1>
        <br>
        <a class="arrow bounce" href="#part1">
        </a>
    </header>




    <section id="part1" class="container text-center my-3 mb-5 bg-transparent">
        <div class="container">
            <h1 class="fontpref2021 pt-3 fontcollorpref2021 text-uppercase">Obrômetro 2.0</h1>
            <hr class="hr-grosso">
            <h5 class="font1 fontcollorpref2021 about">
                   <p style="text-indent: 25px;">
                       A Prefeitura de Manhuaçu, visando dar transparência nas contas públicas e na gestão das ações municipais, implementa o programa de transparência de obras públicas, o Obrômetro 2.0. Este programa representa um novo passo para fornecer informações de status de andamento das obras municipais, executadas com recurso próprio ou por convênios.
                   </p>
                   <p style="text-indent: 25px;">
                       O Obrômetro 2.0 possibilita ao cidadão o acompanhamento de cada investimento em infraestrutura realizado pela prefeitura possibilitando ao contribuinte verificar a aplicação do dinheiro público. As obras serão atualizadas em tempo real, acompanhando o cronograma estabelecido em contrato.
                   </p>
            </h5>
            <hr class="hr-grosso">
        </div>
    </section>



    <section id="part2" class="container text-center my-3 mb-5 bg-transparent">
        <?php
        $sql = "SELECT * FROM tbl_obras where status<>0 ORDER BY data_ts desc";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        $obralista = $consulta->fetchAll();
        $sql=null;
        $consulta=null;

        $count1=0;
            foreach ($obralista as $obras){
                $count1++;
                if($count1 % 2 == 0){
                    //par
                    $pos1="";
                    $pos2="";
                }else{
                    //impar
                    $pos1="order-md-2";
                    $pos2="order-md-1";
                }

                echo "<hr class='my-2'>";
                echo "<div class='row featurette'>";
                    echo "<div class='col-md-5 {$pos1}'>";
                        echo "<h3 class='featurette-heading text-uppercase fontcollorpref2021'>{$obras['obra']}</h3>";
                        echo "<p class='lead text-uppercase fontcollorpref2021'>{$obras['endereco']}</p>";
                        echo "<p class='lead text-uppercase fontcollorpref2021'>INÍCIO DA OBRA:".datahoraBanco2data($obras['inicio_obra'])."</p>";

                            $porc = $obras['valor_parcial'] * 100 / ($obras['valor']+$obras['aditivo']);
                            $porc = number_format($porc, 2);
                            echo "<div class='progress mx-5'>";
                            echo "<div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='{$porc}' aria-valuemin='0' aria-valuemax='100' style='width: {$porc}%'></div>";
                            echo "</div>";
                       echo "<p class='lead text-uppercase fontcollorpref2021'>{$porc}% DA OBRA CONCLUÍDA</p>";

                echo "<a class='btn btn-primary d-grid my-2' href='index.php?pg=Vo&id={$obras['id']}' title=''>";
                echo "SABER MAIS";
                echo "</a>";

                    echo "</div>";


                $sql = "SELECT * FROM `tbl_obras_arquivos` where obra='{$obras['id']}' and tipo='arquivos' and status=1 and home=1 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute();
                $dados = $consulta->fetchAll();//$total[0]
                $sql = null;
                $consulta = null;
                    ?>



                <div class="col-md-7 <?php echo $pos2;?>">
                            <?php
                            $count2=0;
                            foreach ($dados as $dado){
                                $count2++;
                                $link="dados/" . $dado['obra'] . "/" . $dado['tipo'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                echo "<a href='index.php?pg=Vo&id={$obras['id']}'>";
                                    echo "<div class='imagemx ratio ratio-16x9'>";
                                    echo "<img src='{$link}' class='rounded mx-auto d-block img-fluid img-thumbnail' style=' max-height: 400px;' alt='...'>";
                                        if ($obras['status']==2 or $porc>=100){
                                            $carimbo="public/img/carimbo4x3.png" ;
                                            echo "<img src='{$carimbo}' class='rounded mx-auto d-block imagemmascara img-fluid' style=' max-height: 400px;' alt='...'>";
                                        }
                                    echo "</div>";
                                echo "</a>";
                            }
                            ?>
                </div>
                    <?php
                echo "</div>";
            } ?>
    </section>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>
