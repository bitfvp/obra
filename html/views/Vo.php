<?php
$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $obra=fncgetobra($_GET['id']);
}else{
    echo "HOUVE ALGUM ERRO";
}
?>

<style>
    .fundo-1{
        background: #a7b1cc;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #e57e5f, #bc9397, #a7b1cc);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #e57e5f, #bc9397, #a7b1cc); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background-size: 100%;
    }
</style>
<main class="container-fluid fundo-1">

    <header id="head" class="container text-center mb-5 bg-transparent">
        <a href="index.php"><img class="img-fluid img-logo mt-2" src="<?php echo $env->env_estatico; ?>img/PREFEITURA_AZUL.png" alt="" title="<?php echo $env->env_nome; ?>"/></a><br>
        <a href="index.php"><img class="img-fluid img-logo mt-2" src="<?php echo $env->env_estatico; ?>img/Obrometro2azul.png" alt="" title="<?php echo $env->env_nome; ?>"/></a>
        <br>
        <br>
        <br>
        <br>
        <H1 class="fontpref2021 fontcollorpref2021 text-uppercase" style="color: #23425d;">CONFIRA AQUI O STATUS DAS OBRAS MUNICIPAIS</H1>
        <br>
        <a class="arrow bounce" href="#pointofview">
        </a>
    </header>

    <script>
        $(document).ready(function() {
            window.location.href='#pointofview';
        });
    </script>
    <section id="pointofview" class="container my-3 mb-5 bg-transparent">
        <div class='row'>
            <div class='col-md-8 offset-md-2'>
                <div class="card bg-transparent fontcollorpref2021" >
                    <div class="card-body fontpref2021">
                        <h4 class="card-title text-uppercase"><?php echo $obra['obra']; ?></h4>
                        <h5 class="card-subtitle mb-2 text-dark">
                            <?php
                            if ($obra['tipo'] ==1) {
                                $tipo1 = "NOVA OBRA";
                            }
                            if ($obra['tipo'] ==2) {
                                $tipo1 = "REFORMA";
                            }
                            echo $tipo1;

                            $porc = $obra['valor_parcial'] * 100 / ($obra['valor']+$obra['aditivo']);
                            $porc = number_format($porc, 2);

                            if ($obra['status'] == 0) {
                                $status2 = " INATIVO";
                            }
                            if ($obra['status'] ==1) {
                                $status2 = " EM ANDAMENTO";
                            }
                            if ($obra['status'] ==2 or $porc>=100) {
                                $status2 = " CONCLUÍDA";
                            }
                            echo $status2;
                            ?>
                        </h5>
                        <h5 class="card-text text-uppercase">
                            <div class='progress' style="height: 25px;">
                                <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='<?php echo $porc; ?>' aria-valuemin='0' aria-valuemax='100' style='width: <?php echo $porc; ?>%'></div>
                            </div>
                            <h5 class='text-uppercase text-dark'><?php echo $porc; ?>% DA OBRA CONCLUÍDA</h5>
                        </h5>
                        <h5 class="card-text text-uppercase my-0"><i class="fas fa-arrow-right"></i> LOCAL DA OBRA: <?php echo $obra['endereco']; ?></h5>
                        <h5 class="card-text text-uppercase my-0"><i class="fas fa-arrow-right"></i> INÍCIO DA OBRA: <?php echo datahoraBanco2data($obra['inicio_obra']); ?></h5>
                        <h5 class="card-text text-uppercase my-0"><i class="fas fa-arrow-right"></i> PREVISÃO DE CONCLUSÃO: <?php echo $obra['previsao']; ?> DIAS</h5>
                        <h5 class="card-text text-uppercase my-0"><i class="fas fa-arrow-right"></i> VALOR DO CONTRATO: R$<?php echo number_format($obra['valor'],2,',','.'); ?></h5>
                        <h5 class="card-text text-uppercase my-0"><i class="fas fa-arrow-right"></i> ADITIVO DE CONTRATO: R$<?php echo number_format($obra['aditivo'],2,',','.'); ?></h5>
                        <h5 class="card-text text-uppercase my-0"><i class="fas fa-arrow-right"></i> ORIGEM DO RECURSO: <?php echo $obra['recursos']; ?></h5>
                        <h5 class="card-text text-uppercase my-0"><i class="fas fa-arrow-right"></i> ORDENADOR DE DESPESAS: <?php echo $obra['orgao_gestor']; ?></h5>
                        <h5 class="card-text text-uppercase my-0"><i class="fas fa-arrow-right"></i> EXECUTOR: <?php echo $obra['orgao_executor']; ?></h5>

                        <a class='btn btn-primary d-grid my-2' href='index.php#part2' title=''><i class="fas fa-undo-alt fa-2x"></i>VOLTAR</a>
                    </div>
                </div>


            </div>
        </div>
    </section>


    <?php
    $sql = "SELECT * FROM `tbl_obras_arquivos` where obra='{$obra['id']}' and tipo='arquivos' and status=1 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $dados = $consulta->fetchAll();//$total[0]
    $sql = null;
    $consulta = null;
    ?>

    <section id="part1" class="container text-center my-3 mb-5 bg-transparent">
        <div class="row">
            <div class="col-md-8 offset-md-2">

                <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">

                    <?php
                    $count2=0;
                    foreach ($dados as $dado){
                        $count2++;
                        $link="dados/" . $dado['obra'] . "/" . $dado['tipo'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
if ($count2==1){$active="active";}else{$active="";}


                        echo "<div class='carousel-item {$active}'>";
                        echo "<img src='{$link}' class='d-block w-100' style='' alt='...'>";
                        echo "<div class='carousel-caption d-none d-md-block'>";
//                        echo "<h5 class='' style='text-shadow: black 0.1em 0.1em 0.2em;'>{$dado['descricao']}</h5>";
//                        echo "<p  class='text-black' style='text-shadow: white 0.1em 0.1em 0.2em;'>{$dado['descricao']}</p>";
                        echo "</div>";
                        echo "<h5 class=' fontpref2021 fontcollorpref2021' style=''>{$dado['descricao']}</h5>";
                        echo "</div>";

//                        echo "<div class='col-md-4'>";
//                        echo "<a href='#' class='pop'>";
//                        echo "<img src='{$link}' class='ratio ratio-16x9 img-fluid img-thumbnail my-1' style=' ' alt='...'>";
//                        echo "</a>";
//                        echo "</div>";

                    }
                    ?>

                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>

            </div>
        </div>
    </section>
    <script>
        $(function() {
            $('.pop').on('click', function() {
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                $('#imagemodal').modal('show');
            });
        });
    </script>



    <section id="contact" class="container text-center my-3 mb-5 bg-transparent">
        <?php
        if (isset($_SESSION['comentarios']) and $_SESSION['comentarios']==1){}else{?>
            <div class="container fontpref2021">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <h3>Faça um comentário sobre a obra</h3>
                        <form action="index.php?pg=Vo&id=<?php echo $_GET['id']?>&aca=newcomentario" method="post">
                            <div class="row">

                                <div class="col-md-12">
                                    <label for="nome">Nome:</label>
                                    <input id="nome" type="text" class="form-control" name="nome" value=""/>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="mensagem">Mensagem:</label>
                                    <textarea id="mensagem" onkeyup="limite_textarea(this.value,1000,mensagem,'cont')" maxlength="1000"
                                              class="form-control" rows="4" name="mensagem"></textarea>
                                    <span id="cont">1000</span>/1000
                                </div>

                                <div class="col-md-12 d-grid">
                                    <input type="submit" value="SALVAR" class="btn btn-success my-2" />
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </section>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>
