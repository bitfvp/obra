<?php
$page="Login-".$env->env_titulo;
$css="style1";
if(isset($_SESSION['logado']) and ($_SESSION['nivel_acesso']==0 or $_SESSION['nivel_acesso']==1)){
    header("Location: {$env->env_url}aa");
}
include_once("{$env->env_root}includes/head.php");
?>
<style>
    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
    }
</style>
    <main class="container text-center form-signin font3">
        <form action="index.php?pg=Vl&aca=logar" method="post">
            <a href="<?php echo $env->env_url;?>" class="mb-4">
                <img src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="" title="<?php echo $env->env_nome; ?>" style="max-height: 120px;"/>
            </a>
            <h1 class="h3 mb-3 fw-normal">Login</h1>
            <input autofocus type="text" name="syscpf" id="syscpf"  placeholder="CPF" autocomplete="off" class="form-control mb-1" required>
            <script>
                $(document).ready(function(){
                    $('#syscpf').mask('000.000.000-00', {reverse: false});
                });
            </script>
            <input type="password" name="syssenha" id="syssenha"  placeholder="Entre com sua senha" autocomplete="off" class="form-control" required>
            <div class="checkbox mb-3">
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">ENTRAR</button>
        </form>
    </main>


<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>