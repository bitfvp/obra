<?php
function fncobralist(){
    $sql = "SELECT * FROM tbl_obras where status=1 ORDER BY data_ts desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $obralista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $obralista;
}

function fncgetobra($id){
    $sql = "SELECT * FROM tbl_obras WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getobra = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getobra;
}